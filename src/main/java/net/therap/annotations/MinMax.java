package net.therap.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author shabab
 * @since 3/7/18
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface MinMax {

    Evaluator evaluator() default @Evaluator;

    int min() default Integer.MIN_VALUE;

    int max() default Integer.MAX_VALUE;

    int size() default -1;

    String message() default "";

}
