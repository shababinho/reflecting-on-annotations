package net.therap;

import net.therap.models.Person;
import net.therap.utils.AnnotatedValidator;
import net.therap.utils.ValidationError;

import java.util.List;

/**
 * @author shabab
 * @since 3/7/18
 */
public class Main {
    public static void main(String[] args) {
        String dummyName = "Mokles";
        int dummyAge = 35;

        Person person = new Person(dummyName, dummyAge);
        List<ValidationError> errors = AnnotatedValidator.validate(person);
        AnnotatedValidator.print(errors);
    }
}
