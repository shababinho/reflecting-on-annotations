package net.therap.utils;

/**
 * @author shabab
 * @since 3/9/18
 */
public class ArrayError extends ValidationError {
    private static final String ERROR = "ERROR: Array error has occured.";

    public ArrayError() {
        super(ERROR);
    }

    public ArrayError(String message) {
        super(ERROR, message);
    }
}