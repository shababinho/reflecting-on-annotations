package net.therap.utils;

/**
 * @author shabab
 * @since 3/9/18
 */
public class CollectionError extends ValidationError {
    private static final String ERROR = "ERROR: Collection error has occured.";

    public CollectionError() {
        super(ERROR);
    }

    public CollectionError(String message) {
        super(ERROR, message);
    }
}
