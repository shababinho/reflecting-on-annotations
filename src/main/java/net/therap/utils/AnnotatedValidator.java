package net.therap.utils;

import net.therap.annotations.MinMax;
import net.therap.utils.processor.PersonProcessorFactory;
import net.therap.utils.processor.Processor;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shabab
 * @since 3/7/18
 */
public class AnnotatedValidator {

    public static List<ValidationError> validate(Object person) {
        List<ValidationError> errorList = new ArrayList<>();
        try {
            Field fields[] = person.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(MinMax.class)) {
                    field.setAccessible(true);

                    MinMax annotation = field.getAnnotation(MinMax.class);
                    int max = annotation.max();
                    int min = annotation.min();
                    String message = annotation.message();
                    int size = annotation.size();
                    String className = annotation.evaluator().property();

                    Processor<ValidationError, Object> personProcessor = PersonProcessorFactory
                            .createPersonProcessor(className);
                    ValidationError error = personProcessor.process(person, field, min, max, size, message);
                    if (error != null) {
                        errorList.add(error);
                    }

                    field.setAccessible(false);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return errorList;
    }


    public static String template(String message, int min, int max, int size) {
        return message
                .replace("{min}", String.valueOf(min))
                .replace("{max}", String.valueOf(max))
                .replace("{size}", String.valueOf(size));
    }

    public static void print(List<ValidationError> errorList) {
        errorList.stream().forEach(System.err::println);
    }
}

