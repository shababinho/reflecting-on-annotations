package net.therap.utils;

/**
 * @author shabab
 * @since 3/7/18
 */
public class IntegerError extends ValidationError {
    private static final String ERROR = "ERROR: Integer error has occured.";

    public IntegerError() {
        super(ERROR);
    }

    public IntegerError(String message) {
        super(ERROR, message);
    }
}
