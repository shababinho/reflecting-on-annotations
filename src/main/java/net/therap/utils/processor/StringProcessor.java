package net.therap.utils.processor;

import net.therap.utils.AnnotatedValidator;
import net.therap.utils.StringError;
import net.therap.utils.ValidationError;

import java.lang.reflect.Field;

/**
 * @author shabab
 * @since 3/9/18
 */
public class StringProcessor implements Processor<ValidationError, Object> {

    @Override
    public ValidationError process(Object object, Field field, int min, int max, int size, String message) {

        try {

            String value = field.get(object).toString();
            int valueLength = value.length();
            if (size >= 0) {
                if (valueLength != size) {
                    if (message != null) {
                        return new StringError(AnnotatedValidator.template(message, min, max, size));
                    }
                    return new StringError();
                }
            } else {
                if (!(valueLength >= min && valueLength <= max)) {
                    if (message != null) {
                        return new StringError(AnnotatedValidator.template(message, min, max, size));
                    }
                    return new StringError();
                }
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
