package net.therap.utils.processor;

import net.therap.utils.ValidationError;

import java.lang.reflect.Field;

/**
 * @author shabab
 * @since 3/9/18
 */
public interface Processor<T extends ValidationError, E> {
    public T process(E object, Field field, int min, int max, int size, String message);
}
