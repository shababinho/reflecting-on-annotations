package net.therap.utils.processor;

import net.therap.annotations.Evaluator;
import net.therap.annotations.MinMax;
import net.therap.utils.ValidationError;

import java.lang.reflect.Field;

/**
 * @author shabab
 * @since 3/9/18
 */
public class ObjectProcessor implements Processor<ValidationError, Object> {

    @Override
    public ValidationError process(Object object, Field field, int min, int max, int size, String message) {

        //Ignore if there is no evaluator property for an object.
        if (field.getAnnotation(MinMax.class).evaluator().property().length() != 0) {
            Evaluator evaluator = field.getAnnotation(MinMax.class).evaluator();
            String property = evaluator.property();
            try {
                Object propertyObject = field.get(object);
                try {
                    Field propertyField = propertyObject.getClass().getDeclaredField(property);
                    propertyField.setAccessible(true);
                    ValidationError validationError = PersonProcessorFactory
                            .createPersonProcessor(property)
                            .process(propertyObject, propertyField, min, max, size, message);
                    propertyField.setAccessible(false);
                    return validationError;

                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
