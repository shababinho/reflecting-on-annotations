package net.therap.utils.processor;

import net.therap.utils.ValidationError;


/**
 * @author shabab
 * @since 3/9/18
 */
public class PersonProcessorFactory {
    private static final String PACKAGE_NAME = "net.therap.utils.processor";

    @SuppressWarnings(value = "unchecked")
    public static Processor<ValidationError, Object> createPersonProcessor(String className) {
        try {
            Class<?> processorClass = Class.forName(PACKAGE_NAME + "." + className);
            return (Processor<ValidationError, Object>) processorClass.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {

        }
        return new NotSupportedProcessor();
    }
}
