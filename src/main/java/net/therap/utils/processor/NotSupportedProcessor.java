package net.therap.utils.processor;

import net.therap.utils.TypeNotSupportedError;
import net.therap.utils.ValidationError;

import java.lang.reflect.Field;

/**
 * @author shabab
 * @since 3/9/18
 */
public class NotSupportedProcessor implements Processor<ValidationError, Object> {

    @Override
    public ValidationError process(Object object, Field field, int min, int max, int size, String message) {
        return new TypeNotSupportedError();
    }
}
