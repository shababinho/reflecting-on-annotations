package net.therap.utils.processor;

import net.therap.utils.AnnotatedValidator;
import net.therap.utils.IntegerError;
import net.therap.utils.ValidationError;

import java.lang.reflect.Field;

/**
 * @author shabab
 * @since 3/9/18
 */
public class IntegerProcessor implements Processor<ValidationError, Object> {

    @Override
    public ValidationError process(Object object, Field field, int min, int max, int size, String message) {
        try {

            int value = field.getInt(object);

            if (!(value >= min && value <= max)) {
                if (message != null) {
                    return new IntegerError(AnnotatedValidator.template(message, min, max, size));
                }
                return new IntegerError();
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
