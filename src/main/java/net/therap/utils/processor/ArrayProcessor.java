package net.therap.utils.processor;

import net.therap.utils.AnnotatedValidator;
import net.therap.utils.ArrayError;
import net.therap.utils.ValidationError;

import java.lang.reflect.Array;
import java.lang.reflect.Field;

/**
 * @author shabab
 * @since 3/9/18
 */
public class ArrayProcessor implements Processor<ValidationError, Object> {

    @Override
    public ValidationError process(Object object, Field field, int min, int max, int size, String message) {
        try {

            Object arrayObject = (field.get(object));
            int arraySize = Array.getLength(arrayObject);
            if (size >= 0) {
                if (size != arraySize) {
                    if (message != null) {
                        return new ArrayError(AnnotatedValidator.template(message, min, max, size));
                    }
                    return new ArrayError();
                }
            } else {
                if (!(arraySize >= min && arraySize <= max)) {
                    if (message != null) {
                        return new ArrayError(AnnotatedValidator.template(message, min, max, size));
                    }
                    return new ArrayError();
                }
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
