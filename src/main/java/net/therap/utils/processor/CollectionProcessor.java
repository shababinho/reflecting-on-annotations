package net.therap.utils.processor;

import net.therap.utils.AnnotatedValidator;
import net.therap.utils.CollectionError;
import net.therap.utils.ValidationError;

import java.lang.reflect.Field;
import java.util.Collection;

/**
 * @author shabab
 * @since 3/9/18
 */
public class CollectionProcessor implements Processor<ValidationError, Object> {

    @Override
    public ValidationError process(Object object, Field field, int min, int max, int size, String message) {

        try {

            Collection valueCollection = Collection.class.cast(field.get(object));
            int collectionSize = valueCollection.size();

            if (size >= 0) {
                if (collectionSize != size) {
                    if (message != null) {
                        return new CollectionError(AnnotatedValidator.template(message, min, max, size));
                    }
                    return new CollectionError();
                }
            } else {
                if (!(collectionSize >= min && collectionSize <= max)) {
                    if (message != null) {
                        return new CollectionError(AnnotatedValidator.template(message, min, max, size));
                    }
                    return new CollectionError();
                }
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
