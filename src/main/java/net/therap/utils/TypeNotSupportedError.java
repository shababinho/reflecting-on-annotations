package net.therap.utils;

/**
 * @author shabab
 * @since 3/7/18
 */
public class TypeNotSupportedError extends ValidationError {
    private static final String ERROR = "ERROR: Type not supported.";

    public TypeNotSupportedError() {
        super(ERROR);
    }

}
