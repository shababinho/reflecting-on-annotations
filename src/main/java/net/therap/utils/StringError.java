package net.therap.utils;

/**
 * @author shabab
 * @since 3/7/18
 */
public class StringError extends ValidationError {
    private static final String ERROR = "ERROR: String error has occured.";

    public StringError() {
        super(ERROR);
    }

    public StringError(String message) {
        super(ERROR, message);
    }

}
