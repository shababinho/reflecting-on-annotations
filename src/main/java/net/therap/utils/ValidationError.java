package net.therap.utils;

/**
 * @author shabab
 * @since 3/7/18
 */
public abstract class ValidationError {

    private String message;
    private String error;

    ValidationError(String error, String message) {
        this.error = error;
        this.message = message;
    }

    protected ValidationError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean hasMessage() {
        return message != null && !(message.length() == 0);
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getError());
        if (hasMessage()) {
            return builder.append("\n").append(getMessage()).toString();
        }
        return builder.toString();
    }
}
