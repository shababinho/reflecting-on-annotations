package net.therap.models;

import net.therap.annotations.Evaluator;
import net.therap.annotations.MinMax;

/**
 * @author shabab
 * @since 3/7/18
 */
public class Person {

    @MinMax(max = 10, min = 7, evaluator = @Evaluator(property = "StringProcessor"),
            message = "Name cannot be greater than {max} and less than {min}")
    private String name;

    @MinMax(min = 18, max = 30, evaluator = @Evaluator(property = "IntegerProcessor"),
            message = "Age can not be less than {min}")
    private int age;



    public Person(String name, int age) {
        this.name = name;
        this.age = age;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


}
